<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use softDeletes;

    //mass asssignment so we are play with those properties.
    protected $fillable = [
        'title', 'content', 'category_id', 'featured', 'slug', 'user_id',
    ];

    //display image
    public function getFeaturedAttribute($featured)
    {
        return asset($featured);
    }

    //for SoftDelete method so that data we deleted remains in trash, not completely deleted
    protected $dates = ['deleted_at'];

    //one post belongs to one category only.
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}