<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Auth;
use Illuminate\Http\Request;
use Session;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.posts.index')->with('posts', Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //if there is category then only save a post otherwise back to same page.
        $categories = Category::all();
        $tags = Tag::all();

        if ($categories->count() == 0 || $tags->count() == 0) {
            Session::flash('info', 'You must have some categories or tags before attempting to create a post.');
            return redirect()->back();
        }
        return view('admin.posts.create')->with('categories', $categories)
            ->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required|max:255',
            'featured' => 'required|image|mimes:jpeg,png,jpg,gif',
            'content' => 'required',
            'tags' => 'required',
            'category_id' => 'required',
        ]);

        //for storing image
        $featured = $request->featured;
        //at that time getting name of that image which we upload.
        $featured_new_name = time() . $featured->getClientOriginalName();
        //move that featured to uploads/posts in public directory and second parameter is name of the file
        $featured->move(public_path('uploads/posts'), $featured_new_name);

        //for other parts content,title,category_id
        //method 1
        /*
        $post = new Post;
        $post->posts = $request->posts;
        $post->save();
         */

        //method 2
        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'featured' => 'uploads/posts/' . $featured_new_name,
            'category_id' => $request->category_id,
            'slug' => str_slug($request->title),
            'user_id' => Auth::id(),

        ]);

        //post and tag relationship, many to many relationship.Also attach method runs only when pivot table(ie, Post_tag) is created
        $post->tags()->attach($request->tags);

        session()->flash('success', 'Post created successfully');

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        return view('admin.posts.edit')->with('post', $post)->with('categories', Category::all())
            ->with('tags', Tag::all());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //first validate
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);
        //second find the post
        $post = Post::find($id);

        // third if there is image we upload
        if ($request->hasfile('featured')) {
            $featured = $request->featured;
            $featured_new_name = time() . $featured->getClientOriginalName();
            $featured->move('uploads/posts', $featured_new_name);

            $post->featured = 'uploads/posts' . $featured_new_name;
        }
        //fourth then update those database fields
        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        //finally save
        $post->save();

        //newly added tags are selected
        $post->tags()->sync($request->tags);

        Session::flash('success', 'Post updated successfully.');

        return redirect()->route('posts');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->delete();
        Session::flash('success', 'The post was just trashed.');
        return redirect()->back();
    }

    //to view trashed datas
    public function trashed()
    {
        $posts = Post::onlyTrashed()->get();
        return view('admin.posts.trashed')->with('posts', $posts);

    }

    //to permanently delete data from database
    public function kill($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->forceDelete();
        Session()->flash('success', 'Post deleted permanently.');
        return redirect()->back();
    }

    //to restore trashed post
    public function restore($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->restore();
        Session()->flash('success', 'Post restored successfully.');
        return redirect()->route('posts');
    }
}