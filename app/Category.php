<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // same Category has many Post
    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
