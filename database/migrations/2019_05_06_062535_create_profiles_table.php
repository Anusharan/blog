<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //informations about users
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            //Why this fields are nullable?->so that the user is able to edit,delete,create the users
            $table->string('avatar')->nullable();
            //track which user this profile belongs to
            $table->integer('user_id');
            $table->text('about')->nullable();
            $table->string('facebook')->nullable(); https: //www.facebook.com/
            $table->string('youtube')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}