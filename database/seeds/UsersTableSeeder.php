<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Don't need to register for admin

        $user = App\User::create([
            'name' => 'Anusharan',
            'email' => 'anusharan@example.com',
            'password' => bcrypt(env('SERGI_PWD', '123456')),
            'admin' => 1,

        ]);

        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1.png',
            'about' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto itaque mollitia alias esse, accusamus earum tenetur velit ratione eveniet, soluta autem culpa natus. Necessitatibus at, nulla amet asperiores perferendis accusantium?',
            'facebook' => 'facebook.com',
            'youtube' => 'youtube.com',
        ]);

    }
}