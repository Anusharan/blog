<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //calls UserTableSeeder
        // $this->call(UsersTableSeeder::class);
        $this->call(SettingsTableSeeder::class);

    }
}