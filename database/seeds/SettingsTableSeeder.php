<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Setting::create([
            'site_name' => 'Laravel\'s Blog',
            'address' => 'Banepa Nepal',
            'contact_number' => '9800000000',
            'contact_email' => 'anusharan@example.com',
        ]);
    }
}
